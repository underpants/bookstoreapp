﻿using System;
using System.Collections.Generic;
using System.Text;
using BookStore.Models;
using BookStore.Models.Configuration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookStore.Data
{
    //inherits from IDC provides dbset properties needed to
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Book> Books { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // call Book entity to seed data with new Book instance
            // separate this in config file?
            // builder.ApplyConfiguration(new BookConfig());
            builder.Entity<Book>().HasData(
                new Book
                {
                    Id = 1,
                    Name = "Limitless",
                    Author = "Jim Kwik",
                    Genre = "Crime"
                },
                new Book
                {
                    Id = 2,
                    Name = "Tom Sawyer",
                    Author = "John",
                    Genre = "Adventure"
                },
                new Book
                {
                    Id = 3,
                    Name = "Mice and Men",
                    Author = "John Steinbeck",
                    Genre = "Adventure"
                }
                );

            base.OnModelCreating(builder);
        }
    }
}
