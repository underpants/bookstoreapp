﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.Models;
using BookStore.Models.ViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace BookStore.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// Usermanager service with IdentyUser, to manage user of this app by CRUD
        /// inject to Account ctrlr via ctor
        /// SignInManager service to manage user signIn. Both accept generic parameter.
        /// </summary>
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;

        }

        //fetch data rendered here from view
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        //logic to create new user
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel register)
        {
            if (!ModelState.IsValid)
            {
                return View(register);
            }
            //capture email and password via UserManager and SignInManager
            //if user registered call signinasync to sign user
            var user = new IdentityUser { UserName = register.Email, Email = register.Email };
            var result = await _userManager.CreateAsync(user, register.Password);

            if (result.Succeeded)
            {
                //sign user via signin manager
                await _signInManager.SignInAsync(user, isPersistent: false);
                return RedirectToAction("index", "home");
            }
            //loop error and add to ModelState and display in Reg view
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.TryAddModelError(error.Code, error.Description);
                }
            }

            return View(register);
        }

        [HttpGet]
        public IActionResult LogIn()
        {
            return View();
        }

        /// <summary>
        /// signs user in, signinmanager
        /// if successful redirect to home
        /// if unsuccessful display invalid message then render show validation errors to user 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> LogIn(UserLogInViewModel user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }
           
            var result = await _signInManager.PasswordSignInAsync(user.Email, user.Password, user.Remember, false);

            if (result.Succeeded)
            {
                //return to home
                return RedirectToAction("index", "home");
            }
            
            else
            {
                
                    ModelState.TryAddModelError("", "Invalid Username or Password, please try again");
                
            }

            return View();
        }

        /// <summary>
        /// logs out user via post request
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction();
        }

    }
}
