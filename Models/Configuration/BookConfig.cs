﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Models.Configuration
{
    public class BookConfig : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasData(
                new Book
                {
                    Id = 1,
                   Name = "Limitless",
                   Author = "Jim Kwik",
                   Genre = "Crime"
                },
                new Book
                {
                    Id = 2,
                    Name = "Tom Sawyer",
                    Author = "John",
                    Genre = "Adventure"
                },
                new Book
                {
                    Id = 3,
                    Name = "Mice and Men",
                    Author = "John Steinbeck",
                    Genre = "Adventure"
                }
                );
        }
    }
}
